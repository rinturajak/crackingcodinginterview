package com.mycompany.executors;

public class BasicThread {    

    public static void main(final String[] args) throws InterruptedException {
        
        final Thread thread = new Thread() {
            @Override public void run() {
                System.out.println(">>> I am running in a separate thread!");
            }
        };
        
        thread.start();
        thread.join();        

    }    
}