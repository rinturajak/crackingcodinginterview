package com.mycompany.concurrency;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SimpleDirectoryWithExecutor {

    private final ExecutorService executor;
    private SimpleDirectory simpleDirectory;

    public SimpleDirectoryWithExecutor() {
        executor = Executors.newFixedThreadPool(6);
    }

    public long getFilesWithExecutor(final String directory)
            throws InterruptedException, ExecutionException {
        long size = 0;
        simpleDirectory = new SimpleDirectory();
        final Future<Long> sizeInFuture =
                executor.submit(() -> simpleDirectory.getFiles(directory));
        size = size + sizeInFuture.get();
        return size;
    }

    @PostConstruct
    public void destroy() {
        executor.shutdown();
    }
}
