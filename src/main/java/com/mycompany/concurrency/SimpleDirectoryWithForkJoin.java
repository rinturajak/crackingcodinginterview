package com.mycompany.concurrency;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SimpleDirectoryWithForkJoin {

    private final ForkJoinPool executor;

    public SimpleDirectoryWithForkJoin() {
        executor = new ForkJoinPool(4);
    }

    public long getFilesWithForkJoin(final String directory) {
        final Path directoryPath = Paths.get(directory);

        if (!Files.isDirectory(directoryPath)) {
            throw new IllegalArgumentException("Please Enter Valid Dicectory Location");
        }
        return executor.invoke(new FolderSizeTask(directoryPath));
    }

    @PostConstruct
    public void destroy() {
        executor.shutdown();
    }

    @SuppressWarnings("serial")
    class DocumentSizeTask extends RecursiveTask<Long> {
        Path document;

        DocumentSizeTask(final Path document) {
            super();
            this.document = document;
        }

        @Override
        protected Long compute() {
            try {
                final long size = Files.size(document);
                log.debug("Size: " + size);
                return size;
            } catch (final IOException e) {
                return 0L;
            }
        }
    }

    @SuppressWarnings("serial")
    class FolderSizeTask extends RecursiveTask<Long> {
        private final Path folder;

        FolderSizeTask(final Path folder) {
            super();
            this.folder = folder;
        }

        @Override
        protected Long compute() {
            long count = 0L;
            try {
                final List<RecursiveTask<Long>> forks = new LinkedList<>();

                for (final Path document : Files.newDirectoryStream(folder)) {
                    final DocumentSizeTask task = new DocumentSizeTask(document);
                    forks.add(task);
                    task.fork();
                }
                for (final RecursiveTask<Long> task : forks) {
                    count = count + task.join();
                }
            } catch (final Exception ignore) {

            }
            log.debug("Size: " + count);
            return count;
        }
    }
}
