package com.mycompany.concurrency;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SimpleDirectory {

    public long getFiles(final String directory) throws IOException {
        long size = 0;
        final Path directoryPath = Paths.get(directory);

        if (!Files.isDirectory(directoryPath)) {
            throw new IllegalArgumentException("Please Enter Valid Dicectory Location");
        }

        final DirectoryStream<Path> filesPath = Files.newDirectoryStream(directoryPath);
        for (final Path filePath : filesPath) {
            size = size + Files.size(filePath);
        }
        //log.debug("SimpleDirectory-> Size: " + size);
        return size;

    }

}
