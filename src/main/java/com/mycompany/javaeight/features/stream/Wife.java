package com.mycompany.javaeight.features.stream;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Wife {
    
    private String name;
    private List<Children> childrens = new ArrayList<>();
}
