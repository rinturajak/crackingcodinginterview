package com.mycompany.javaeight.features.stream;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PrimeMinisterOfIndia {
    
    private String firstName;
    private String lastName;
    private int electionYear;
    private String party;
    private List<Wife> wife; 

}
