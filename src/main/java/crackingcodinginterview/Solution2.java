package crackingcodinginterview;

import java.util.Scanner;

public class Solution2 {
    public static void main(final String args[]) throws Exception {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        final Scanner in = new Scanner(System.in);
        final String line = in.nextLine();
        final String printString = passwordStrength(line);
        
        System.out.println(printString);
        
    }

    private static String passwordStrength(final String password) {
        final String digitRegex = ".*\\d.*";
        final String specialCharatersRegex = ".*[!#$*].*";
        final String lowerCaseRegex = ".*[a-z].*";
        final String upperCaseRegex = ".*[A-Z].*";
        int counter = 0;
        if (password.length() < 6)  {
            //
            counter = 0;
        } else {
            if (password.matches(digitRegex)) {
                System.out.println("In digit");
                counter++;
            } 
            if (password.matches(specialCharatersRegex)) {
                System.out.println("specialCharatersRegex");
                counter++;
            } 
            if (password.matches(lowerCaseRegex)) {
                System.out.println("lowerCaseRegex");
                counter++;
            } 
            if (password.matches(upperCaseRegex)) {
                System.out.println("upperCaseRegex");
                counter++;
            }
        }
        
        if (counter == 1) {
            return "very weak";
        } else if (counter == 2) {
            return "weak";
        } else if (counter == 3) {
            return "strong";
        } else if (counter == 4) {
            return "very strong";
        } else {
            return "password should be more than 5 letters";
        }
    }
}
