package crackingcodinginterview;

public class ReverseString {
    
    public static String reverString(final String inputString) {
        if (inputString != null && inputString.length() > 1) {
          final char[] inputStringChars =  inputString.toCharArray();
          int right = inputStringChars.length - 1;
          for (int left = 0; left < right; left++, right--) {
              final char c = inputStringChars[left];
              inputStringChars[left] = inputStringChars[right];
              inputStringChars[right] = c;
          }
          return new String(inputStringChars);
        }
        return inputString;
    }
    
    public static String reverseString1(final String inputString) {
        if (inputString != null && inputString.length() > 1) {
            final int length = inputString.length();
            final char[] chars = new char[length];
            for (int i = 0; i <= length / 2 ; i++) {
                final int j = length - 1 - i;
                chars[i] = inputString.charAt(j);
                chars[j] = inputString.charAt(i);
            }
            return new String(chars);
        }
        return inputString;
    }
    
    public static void main(final String[] args) {
        System.out.println(reverString("String"));
        System.out.println(reverString("s"));
        System.out.println(reverString("ab"));
        System.out.println(reverString(null));
        System.out.println(reverString(Integer.toString(1234)));
        
        System.out.println(reverseString1("Strin"));
        
    }
}
