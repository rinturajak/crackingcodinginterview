package crackingcodinginterview;

import java.util.Scanner;

public class WildcardMatching {

    public static void main(final String args[]) throws Exception {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        final Scanner in = new Scanner(System.in);
        final String str1 = in.nextLine();
        final String str2 = in.nextLine();
        in.close();
        final boolean matched = stringMatcherWithWildcard(str1, str2);

        if (matched) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }

    }

    public static boolean stringMatcherWithWildcard(final String str1, final String str2) {
        if (str1.length() != str2.length()) {
            return false;
        }

        for (int i = 0; i < str1.length(); i++) {
            if (!(str1.toLowerCase().charAt(i) == '?' || str2.toLowerCase().charAt(i) == '?')) {
                if (str1.toLowerCase().charAt(i) != str2.toLowerCase().charAt(i)) {
                    return false;
                }
            }

        }
        return true;
    }
}
