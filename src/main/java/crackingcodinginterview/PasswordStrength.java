package crackingcodinginterview;

import java.util.Scanner;

public class PasswordStrength {

    public static void main(final String args[]) throws Exception {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        final Scanner in = new Scanner(System.in);
        final String line = in.nextLine();
        in.close();
        final String printString = passwordStrength(line);

        System.out.println(printString);

    }

    private static String passwordStrength(final String password) {
        final String digitRegex = ".*[0-9].*";
        final String specialCharatersRegex = ".*[!#$*].*";
        final String lowerCaseRegex = ".*[a-z].*";
        final String upperCaseRegex = ".*[A-Z].*";
        int strength = 0;
        
        if (password.length() < 6) {
            strength = 0; 
        }
        
        if (password.matches(digitRegex)) {
            strength++;
        }
        if (password.matches(specialCharatersRegex)) {
            strength++;
        }
        if (password.matches(lowerCaseRegex)) {
            strength++;
        }
        if (password.matches(upperCaseRegex)) {
            strength++;
        }

        if (strength == 1) {
            return "very weak";
        } else if (strength == 2) {
            return "weak";
        } else if (strength == 3) {
            return "strong";
        } else if (strength == 4) {
            return "very strong";
        } else {
            return "password should be more than 5 letters";
        }
    }

}
