package crackingcodinginterview;

public class ShiftArray {

    public static int[] shiftLeft(final int[] input, final int shift) {
        final int length = input.length;
        final int[] output = new int[length];
        for (int i = 0; i < length; i++) {
            output[i] = input[(length + shift + i) % length];
            //output[((length - (shift - i)) % length)] = input[i]; // Remember Mod as it should fit within the same size
            //System.out.println("o[i]: "+i+ " in[i]: "+((length - (shift - i)) % length)+ ": "+output[i]);
        }
        return output;
    }

    public static void main(final String[] args) {
        final int[] input = { 1, 2, 3, 4, 5};
        final int[] output = shiftLeft(input, 2);

        for (int i = 0; i < output.length; i++) {
            System.out.print(output[i] + " ");
        }

    }


}
