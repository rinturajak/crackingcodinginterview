package crackingcodinginterview;

public class SubString {

    public static boolean subStringWithIndexOf(final String str, final String subStr) {
        if (str.indexOf(subStr) != -1) {
            return true;
        }
        return false;
    }

    public static boolean subString(final String str, final String subStr) {
        int j = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == subStr.charAt(j)) {
                j++;
            } else {
                j = 0;
            }

            if (j == subStr.length()) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isRotation(final String str1, final String str2) {
        final String concates = str1 + str1; 
        return subString(concates, str2);
    }

    public static void main(final String[] args) {
        System.out.println(subStringWithIndexOf("RintuRajak", "Rajak")); // true
        System.out.println(subString("RintuRajak", "Rajak")); // true
        System.out.println(isRotation("pleap", "apple")); //true
    }
}
