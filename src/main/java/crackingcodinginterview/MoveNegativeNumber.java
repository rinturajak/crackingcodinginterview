package crackingcodinginterview;

public class MoveNegativeNumber {
    
    public static int[] move(final int[] input)  {
        int negativeCounter = 0;
        int temp = 0;
        for (int i=0; i<input.length; i++) {
            if (input[i] < 0) {
                temp = input[negativeCounter];
                input[negativeCounter] = input[i];
                input[i] = temp;
                negativeCounter++;
            }
        }
        return input;
    }
    
    public static void main(final String[] args) {
        final int[] input = {-1, 2, 3, -2, -4, 5, -9, 9, 10};
        
        final int[] output = move(input);
        
        for (int i =0; i<output.length; i++) {
            System.out.println(output[i]);
        }
    }

}
