package crackingcodinginterview;

public class RemoveDuplicateCharacter {

    public static String removeDuplicates(final String str) {
        if (str != null && str.length() > 1) {
            final char[] cs = str.toLowerCase().toCharArray();
            final StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < cs.length; i++) {
                boolean flag = false;
                for (int j = i + 1; j < cs.length; j++) {
                    if ((cs[i] != '\0' || cs[j] != '\0') && (cs[i] == cs[j])) {
                        cs[j] = '\0';
                        flag = true;
                    }
                }
                if (!flag && cs[i] != '\0') {
                    buffer.append(cs[i]);
                }
            }
            return buffer.toString();
        }
        return str;
    }

    public static void main(final String[] args) {
        System.out.println(removeDuplicates("stringingingabddddc vgjgf")); //strabc vjf
    }

}
