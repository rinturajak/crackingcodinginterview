package crackingcodinginterview;

public class CheckDuplicate {
    
    public static boolean isDuplicate(final String inputString) {
        if (inputString != null && inputString.length() > 1) {
            final int length = inputString.length();
            final char[] chars = inputString.toLowerCase().toCharArray();
            for (int i = 0; i < length-2; i++) {
                for( int j = i+1; j < length; j++) {
                    if (chars[i] == chars[j]) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public static void main(final String[] args) {
        System.out.println(isDuplicate("Strings"));
        System.out.println(isDuplicate("s"));
        System.out.println(isDuplicate("aabb"));
        System.out.println(isDuplicate("aaa"));
        System.out.println(isDuplicate(""));
        System.out.println(isDuplicate(null));
    }

}
