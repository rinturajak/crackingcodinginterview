package crackingcodinginterview;

import java.util.Scanner;

public class Solution {
    public static void main(final String args[]) throws Exception {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        final Scanner in = new Scanner(System.in);
        final String line = in.nextLine();
        final String printString = printString(line);
        
        final String[] tokens = printString.split(" ");
        for (int i=tokens.length-1; i>=0; i--) {
            System.out.println(tokens[i]);
        }
        
    }

    private static String printString(final String text) {
        final int indexOfT = text.indexOf("t");
        System.out.println("indexOfT: " +indexOfT);
        if (indexOfT >= 0) {
            if (indexOfT % 2 == 0) {
                return reverseString(text);
            } else {
                return text;
            }
        } else {
            return "None";
        }
    }

    public static String reverseString(final String inputString) {
        if (inputString != null && inputString.length() > 1) {
            final int length = inputString.length();            
            final char[] chars = new char[length];
            
            for (int i = 0; i <= length / 2 ; i++) {
                final int j = length - 1 - i;
                chars[i] = inputString.charAt(j);
                chars[j] = inputString.charAt(i);
            }
            return new String(chars);
        }
        return inputString;
    }
}
