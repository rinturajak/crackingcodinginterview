package crackingcodinginterview;

import java.util.Scanner;

public class HackerRankSampleRun {

    public static void main(final String[] args) {
        final Scanner in = new Scanner(System.in);
        final int n = in.nextInt();
        System.out.println(n);
        final int k = in.nextInt();
        System.out.println(k);
        final int a[] = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
        
        int o[] = new int[n];
        o = shiftLeft(a, k);
        
        for (int i = 0;  i < n; i++) {
            System.out.print(o[i]+" ");
        }
    }
    
    public static int[] shiftLeft(final int[] input, final int shift) {
        final int length = input.length;
        final int[] output = new int[length];
        for (int i = 0; i < length; i++) {
            output[i] = input[(length + shift + i) % length]; // Remember Mod as it should fit within the same size
        }
        return output;
    }
}

