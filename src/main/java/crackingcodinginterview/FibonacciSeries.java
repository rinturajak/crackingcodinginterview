package crackingcodinginterview;

import java.math.BigInteger;

public class FibonacciSeries {
    
    /**
     * The method return the nth number in the Fibonacci Series
     * @param nthPosition
     * @return BigInteger
     */
    public static BigInteger getFibonacciNumber(final int nthPosition) {
        BigInteger fn2Number = new BigInteger("0");
        BigInteger fn1Number = new BigInteger("1");
        BigInteger fnNumber = new BigInteger("0");
        
        if (nthPosition == 0) {
            return fn2Number;
        } else if (nthPosition == 1) {
            return fn1Number;
        }
        
        for (int i = 2; i < nthPosition; i++) {
            fnNumber = fn2Number.add(fn1Number);
            fn2Number = fn1Number;
            fn1Number = fnNumber;
        }
        return fnNumber;
    }
    
    public static void main(final String[] args) {
        System.out.println(getFibonacciNumber(100)); //218922995834555169026
    }
}
