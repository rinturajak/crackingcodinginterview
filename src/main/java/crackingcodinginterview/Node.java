package crackingcodinginterview;

public class Node {

    private Node left, right;
    int data;

    public Node(final int data) {
        this.data = data;
    }

    public void insert(final int value) {
        if (value <= data) {
            if (left == null) {
                left = new Node(value);
            } else {
                left.insert(value);
            }
        } else {
            if (right == null) {
                right = new Node(value);
            } else {
                right.insert(value);
            }
        }
    }

    public boolean contains(final int value) {
        if (value == data) {
            return true;
        } else if (value < data) {
            if (left == null) {
                return false;
            } else {
                return left.contains(value);
            }
        } else {
            if (right == null) {
                return false;
            } else {
                return right.contains(value);
            }
        }
    }

    public static int maxDepth(final Node root) {
        if (root == null) {
            return 0;
        }
        return 1 + Math.max(maxDepth(root.left), maxDepth(root.right));
    }
    
    public static int minDepth(final Node root) {
        
        if (root == null) {
            return 0;
        }
        System.out.println(root.data);
        return 1 + Math.min(minDepth(root.left), minDepth(root.right));
    }
    
    public static boolean isBalanced(final Node root){
        return (maxDepth(root) - minDepth(root) <= 1);
      }
    
    public static void main(final String[] args) {
        final Node root = new Node(3);
        
        root.left = new Node(5);
        root.left.left = new Node(1);
        root.left.right = new Node(4);
        
        root.right = new Node(2);
        root.right.left= new Node(6);
        
        System.out.println(isBalanced(root));
    }

}
