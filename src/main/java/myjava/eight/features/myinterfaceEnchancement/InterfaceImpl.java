package myjava.eight.features.myinterfaceEnchancement;

public class InterfaceImpl implements Interface1, Interface2 {

    @Override
    public void method1() {
        System.out.println("Hello from "+this);
        
    }
    
    @Override
    public void defaultMethod1() {
        Interface2.super.defaultMethod1();
    }



    public static void main(final String[] args) {
        final InterfaceImpl interfaceImpl = new InterfaceImpl();
        interfaceImpl.method1();
        interfaceImpl.defaultMethod1();
    }

}
