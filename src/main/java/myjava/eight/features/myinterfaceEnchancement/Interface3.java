package myjava.eight.features.myinterfaceEnchancement;

public interface Interface3 extends Interface1 {
    
    @Override
    default void defaultMethod2() {
        System.out.println("I am in Interface 3");
    }

}
