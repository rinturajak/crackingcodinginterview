package myjava.eight.features.myinterfaceEnchancement;

public interface Interface1 {
    
    void method1();
    
    default void defaultMethod1() {
        System.out.println("Hello from "+ this);
    }
    
    default void defaultMethod2() {
        System.out.println("Hi Dear "+ this);
    }

}
