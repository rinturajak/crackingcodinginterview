package myjava.eight.features.myinterfaceEnchancement;

public interface Interface2 {
    
    void method1();
    
    default void defaultMethod1() {
        System.out.println("Hello from "+ this);
    }

}
