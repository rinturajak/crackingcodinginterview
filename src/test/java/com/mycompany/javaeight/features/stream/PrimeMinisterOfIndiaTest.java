package com.mycompany.javaeight.features.stream;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.Test;

public class PrimeMinisterOfIndiaTest {

    List<Wife> wifes = Arrays.asList(new Wife(
            "Katrina", Arrays.asList(new Children("Rohan", 34), 
                                    new Children("Rajesh", 42), 
                                    new Children("Pawan", 26))),
            new Wife(
                    "Sunny", Arrays.asList(new Children("Ajay", 27), 
                                            new Children("Salman", 50), 
                                            new Children("Shahruk", 42))),
            new Wife(
                    "Deepika", Arrays.asList(new Children("Ranbeer", 35), 
                                            new Children("Maliya", 43), 
                                            new Children("Anil", 39)))
            );
    public List<PrimeMinisterOfIndia> primeMinisters =
            Arrays.asList(new PrimeMinisterOfIndia("Narendra", "Modi", 2014, "BJP", wifes),
            new PrimeMinisterOfIndia("Manmohan", "Singh", 2009, "INC", Arrays.asList(new Wife())),
            new PrimeMinisterOfIndia("Manmohan", "Singh", 2004, "INC", Arrays.asList(new Wife())),
            new PrimeMinisterOfIndia("Atal", "Vajpayee", 1998, "BJP", Arrays.asList(new Wife())),
            new PrimeMinisterOfIndia("Inder", "Gujral", 1997, "United Front", Arrays.asList(new Wife())),
            new PrimeMinisterOfIndia("Deve", "Gowda", 1996, "Janata Dal", Arrays.asList(new Wife())),
            new PrimeMinisterOfIndia("Narasimha", "Rao", 1991, "INC", Arrays.asList(new Wife())),
            new PrimeMinisterOfIndia("Chandra", "Shekhar", 1990, "Samajwadi Janata Party", Arrays.asList(new Wife())),
            new PrimeMinisterOfIndia("Vishwanath", "Singh", 1989, "Janata Dal", Arrays.asList(new Wife())),
            new PrimeMinisterOfIndia("Rajiv", "Gandhi", 1984, "INC", Arrays.asList(new Wife())));
                    
    
    @Test
    public void filter_map_limit_distinct_collect() {
        primeMinisters.stream()
            .filter(primeMinister -> primeMinister.getElectionYear() < 2000)
            .map(PrimeMinisterOfIndia::getLastName)
            .distinct()
            .limit(3)
            .collect(Collectors.toList())
            .forEach(primeMinister -> System.out.println(primeMinister));
    }
    
    @Test
    public void flatmap_sort_collect() {
        final PrimeMinisterOfIndia modi = primeMinisters.get(0);
         final Map<Integer, Children> childrens = modi.getWife().stream()
            .flatMap(wife -> wife.getChildrens().stream())
            .sorted(Comparator.comparing(Children::getAge).reversed())
            .collect(Collectors.toMap(Children::getAge, Function.identity(), (age1, age2) -> { return age1;})); //merge function to handle duplicates in map keys
        
        System.out.println(childrens);
    }
}
