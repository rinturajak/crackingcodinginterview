package com.mycompany.concurrency;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SimpleDirectoryTest {

    @Test
    public void testGetFiles() throws IOException {
        final SimpleDirectory simpleDirectory = new SimpleDirectory();
        final long start = System.nanoTime();
        long filesSize = 0;
        for (int i = 0; i < 40000; i++) {
            filesSize = filesSize + simpleDirectory.getFiles("E:\\datazi\\help\\help\\Java");
        }
        final long taken = System.nanoTime();
        final long elapsedTime = taken - start;

        log.debug("filesSize: " + filesSize + " Start Time: " + start + ", endTime: " + taken
                + ", Elapsed(sec): " + TimeUnit.NANOSECONDS.toSeconds(elapsedTime));
    }
}
